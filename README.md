# sftp_test

Flutter SFTP test

## Packages

- https://pub.dev/packages/file : Un paquet pour manipuler les fichiers et système de fichier
- https://pub.dev/packages/file_picker : Widget dialog pour selectionner/sauvegarder des fichiers (desktop uniquement)
- https://pub.dev/packages/path : Manipulations de chaîne de caractère contenant des chemins de fichiers
- https://pub.dev/packages/path_provider : Permet d'obtenir des chemins système (appdata, temp, ...)
- https://pub.dev/packages/humanizer : Formattage des valeurs pour les rendre lisibles + conversion d'unités de mesures auto

## Doc

- https://blog.logrocket.com/drag-and-drop-ui-elements-in-flutter-with-draggable-and-dragtarget/
- 

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
