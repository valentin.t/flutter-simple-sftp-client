import 'package:flutter/material.dart';

import '../screens/home.dart';
import '../screens/file_edit.dart';

final Map<String, WidgetBuilder> allRoutes = <String, WidgetBuilder>{
  "/": (BuildContext context) => const MyHomePage(),
  "/edit": (BuildContext context) => const FileEdit(),
};