import 'package:flutter/material.dart';

import '../services/sftp_service.dart';

class FileEdit extends StatefulWidget {
  const FileEdit({Key? key}) : super(key: key);

  @override
  State<FileEdit> createState() => _FileEditState();
}

class _FileEditState extends State<FileEdit> {
  late SftpService _client;
  late TextEditingController _controller;
  late String _path;

  @override
  Widget build(BuildContext context) {
    var args = ModalRoute.of(context)!.settings.arguments as List<dynamic>;
    _client = args[0];
    _path = args[1];
    _controller = TextEditingController(text: args[2]);

    return Scaffold(
      body: Form(
        child: Scaffold(
        appBar: AppBar(
          title: Text('Contenu du fichier ${_path.substring(_path.lastIndexOf('/') + 1)}'),
          centerTitle: true,
        ),
          body: TextFormField(
              controller: _controller,
              minLines: null,
              maxLines: null,
              expands: true,
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: _save,
            tooltip: 'Sauvegarder',
            child: const Icon(Icons.save),
          ),
        ),
      ),
    );
  }

  void _save() {
    String text = _controller.value.text;

    _client.writeRemoteFile(_path, text).
    whenComplete(() {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Fichier sauvegardé')));
      Navigator.pop(context);
    });

  }
}
