import 'dart:io';

import 'package:flutter/material.dart';
import 'package:dartssh2/dartssh2.dart';
import 'package:file_picker/file_picker.dart';
import 'package:humanizer/humanizer.dart';
import 'package:path/path.dart';
import 'package:sftp_test/dialog_manager.dart';

import '../environment/environment.dart';

import '../services/sftp_service.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  SftpService? _client;
  String _currentPath = '/';
  late List<SftpName> _fileTree;

  @override
  void initState() {
    super.initState();
    _getClient();
  }

  @override
  void dispose() {
    _client?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Visibility(
          visible: _currentPath != '/',
          child: DragTarget<SftpName>(
            builder: (context, candidateData, rejectedData) => IconButton(
              icon: const Icon(Icons.arrow_back),
              tooltip: 'Revenir au répertoire parent',
              onPressed: _goBack,
            ),
            onAccept: (data) => _onItemDropped('..', data),
          ),
        ),
        centerTitle: true,
        title: Text('Affichage du répertoire $_currentPath'),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            tooltip: 'Recharger',
            onPressed: () => setState(() {}),
          ),
        ],
      ),
      body: _client == null
          ? Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                  CircularProgressIndicator(),
                  Text('Connexion en cours...')
                ]))
          : FutureBuilder<List<SftpName>>(
              future: _client!.listDirectory(_currentPath),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(child: CircularProgressIndicator());
                } else if (snapshot.hasError) {
                  return Center(
                      child: Text('Erreur : ${snapshot.error.toString()}'));
                }

                _fileTree = snapshot.data!;

                return _getScreenContent();
              },
            ),
      floatingActionButton: Visibility(
          visible: _client != null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FloatingActionButton(
                tooltip: 'Envoyer un fichier',
                child: const Icon(Icons.upload_file),
                onPressed: () async => _uploadFile(),
              ),
              /*const SizedBox(width: 35),
              FloatingActionButton(
                tooltip: 'Envoyer un dossier',
                child: const Icon(Icons.drive_folder_upload),
                onPressed: () async => _uploadFile(),
              ),*/
            ],
          )),
    );
  }

  Widget _getScreenContent() {
    if (_fileTree.isEmpty) {
      return const Center(child: Text('Répertoire vide'));
    }

    return ListView.separated(
        itemCount: _fileTree.length,
        separatorBuilder: (context, index) => const Divider(),
        itemBuilder: (context, index) {
          final item = _fileTree[index];
          String size = item.attr.isDirectory ? '' : '${InformationSizeFormat(locale: 'FR-fr').format(item.attr.size!.bytes())}\n';
          String lastModifiedDate = item.attr.modifyTime != null
              ? 'Dernière modification le ${DateTime.fromMillisecondsSinceEpoch(item.attr.modifyTime! * Duration.millisecondsPerSecond)}'
              : '';

          var tile = ListTile(
            leading: item.attr.isDirectory
                ? const Icon(Icons.folder)
                : const Icon(Icons.feed),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                    icon: const Icon(Icons.download),
                    tooltip: 'Télécharger',
                    onPressed: () => _downloadElement(item)),
                const SizedBox(width: 15),
                IconButton(
                    icon: const Icon(Icons.delete),
                    tooltip: 'Supprimer',
                    onPressed: () => _deleteElement(item)),
                const SizedBox(width: 15),
                IconButton(
                    icon: const Icon(Icons.edit),
                    tooltip: 'Renommer',
                    onPressed: () => _renameElement(item)),
              ],
            ),
            title: Text(item.filename),
            subtitle: Text('$size$lastModifiedDate'),
            onTap: () => _onItemClicked(item),
          );
          var draggableTile = Draggable<SftpName>(
              data: item,
              feedback: Material(
                  child: ConstrainedBox(
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width),
                      child: tile)),
              childWhenDragging: Container(),
              child: tile);

          return item.attr.isDirectory
              ? DragTarget<SftpName>(
                  builder: (context, candidateData, rejectedData) =>
                      draggableTile,
                  //onWillAccept: (data) => true,
                  onAccept: (data) => _onItemDropped(item.filename, data),
                )
              : draggableTile;
        });
  }

  void _onItemClicked(SftpName item) async {
    if (item.attr.isDirectory) {
      var newPath = _currentPath;

      if (_currentPath != '/') {
        newPath += '/';
      }

      newPath += item.filename;
      _changePath(newPath);
    } else {
      String path = '$_currentPath/${item.filename}';
      String content = await _client!.readRemoteFile(path);
      Navigator.pushNamed(this.context, '/edit', arguments: <dynamic>[_client, path, content]);
    }
  }

  void _onItemDropped(String directoryName, SftpName item) async {
    _client!.move('$_currentPath/${item.filename}',
        '$_currentPath/$directoryName/${item.filename}');
    setState(() {});
  }

  void _goBack() {
    var newPath =
        _currentPath.replaceRange(_currentPath.lastIndexOf('/'), null, '');
    if (newPath.isEmpty) {
      newPath = '/';
    }

    _changePath(newPath);
  }

  Future<void> _deleteElement(SftpName item) async {
    var path = '$_currentPath/${item.filename}';

    if (item.attr.isDirectory) {
      await _client!.deleteDirectory(path);
    } else {
      await _client!.deleteFile(path);
    }

    setState(() {});
  }

  Future<void> _renameElement(SftpName item) async {
    var path = '$_currentPath/${item.filename}';
    String name = item.filename;
    var isRenaming = await DialogManager.promptDialog(
        this.context, 'Renommer', 'Nouveau nom de l\'élément :', name);

    if (isRenaming.key) {
      _client!.rename(path, isRenaming.value);
      setState(() {});
    }
  }

  Future<void> _uploadFile() async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(dialogTitle: 'Sélectionnez un fichier');
    if (result == null) {
      return;
    }
    final File file = File(result.files.first.path!);
    final String filePath = basename(file.path);

    ScaffoldMessenger.of(this.context).showSnackBar(const SnackBar(
      content: Text('Transfert en cours...'),
      dismissDirection: DismissDirection.none,
      duration: Duration(days: 100),
    ));
    await _client!.uploadFile(file, '$_currentPath/$filePath');

    setState(() {
      ScaffoldMessenger.of(this.context).clearSnackBars();
      ScaffoldMessenger.of(this.context)
          .showSnackBar(const SnackBar(content: Text('Fichier envoyé.')));
    });
  }

  Future<void> _downloadElement(SftpName item) async {
    var remotePath = '$_currentPath/${item.filename}';
    String? localPath = await FilePicker.platform.saveFile(
        dialogTitle:
            'Emplacement du ${item.attr.isDirectory ? 'dossier' : 'fichier'}',
        fileName: item.filename);

    if (localPath == null) {
      return;
    }

    if (item.attr.isDirectory) {
      await _client!.downloadDirectory(remotePath, localPath);
    } else {
      await _client!.downloadFile(remotePath, localPath);
    }

    ScaffoldMessenger.of(this.context)
        .showSnackBar(const SnackBar(content: Text('Élément téléchargé.')));
  }

  Future<void> _getClient() async {
    _client = await SftpService.create(
        host: HOST_SFTP,
        port: PORT_SFTP,
        username: USERNAME_SFTP,
        password: PASSWORD_SFTP);
    setState(() {});
  }

  void _changePath(String newPath) {
    setState(() => _currentPath = newPath);
  }
}
