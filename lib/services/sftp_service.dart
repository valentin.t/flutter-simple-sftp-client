import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';

import 'package:file/memory.dart';
import 'package:dartssh2/dartssh2.dart';

class SftpService {
  late String _host;
  late int _port;
  late String _username;
  late String _password;
  late final SSHClient _sshClient;
  late final SftpClient _stfpClient;

  SftpService(this._host, this._port, this._username, this._password,
      this._sshClient, this._stfpClient);

  SftpService._create(String host, int port, String username, String password) {
    _host = host;
    _port = port;
    _username = username;
    _password = password;
  }

  Future<void> _asyncInit() async {
    var socket = await SSHSocket.connect(_host, _port);

    _sshClient = SSHClient(socket,
        username: _username, onPasswordRequest: () => _password);

    _stfpClient = await _sshClient.sftp();
  }

  static Future<SftpService> create(
      {required String host,
      required int port,
      required String username,
      required String password}) async {
    var instance = SftpService._create(host, port, username, password);
    await instance._asyncInit();

    return instance;
  }

  Future<List<SftpName>> listDirectory(String path,
      [bool removeRootAndParent = true]) async {
    var files = await _stfpClient.listdir(path);

    if (removeRootAndParent) {
      if (files.isNotEmpty && files.length >= 2) {
        // On dégage . et ..
        files.removeRange(0, 2);
      }
    }

    return files;
  }

  Future<void> uploadFile(File uploadedFile, String remoteFilePath) async {
    var file = await _stfpClient.open(remoteFilePath,
        mode: SftpFileOpenMode.create |
            SftpFileOpenMode.truncate |
            SftpFileOpenMode.write);

    await file.write(uploadedFile.openRead().cast());
    await file.close();
  }

  Future<void> uploadDirectory(String localPath, String remotePath) async {
    //flemme
  }

  Future<void> downloadFile(String remotePath, String localPath) async {
    var remoteFile = await _stfpClient.open(remotePath);
    var content = await remoteFile.readBytes();

    var localFile = File(localPath);
    await localFile.writeAsBytes(content);
  }

  Future<void> downloadDirectory(
      String remoteDirPath, String localDirPath) async {
    var remoteDirContent = await listDirectory(remoteDirPath);
    var localDir = Directory(localDirPath);

    if (!localDir.existsSync()) {
      localDir = await localDir.create(recursive: true);
    }

    for (final item in remoteDirContent) {
      var remotePath = '$remoteDirPath/${item.filename}';
      var localPath = '${localDir.path}/${item.filename}';

      if (item.attr.isDirectory) {
        await downloadDirectory(remotePath, localPath);
      } else {
        await downloadFile(remotePath, localPath);
      }
    }
  }

  Future<void> copyFile(String remoteFilePath, String newFilePath) async {
    var file = await _stfpClient.open(remoteFilePath);
    var fileContent = await file.readBytes();
    await file.close();

    var fileToCopy =
        await MemoryFileSystem().file('temp').writeAsBytes(fileContent);

    await uploadFile(fileToCopy, newFilePath);
  }

  Future<void> copyDirectory(String remoteFilePath, String newFilePath) async {
    //flemme
    //await uploadDirectory('', '');
  }

  Future<void> deleteFile(String path) async {
    await _stfpClient.remove(path);
  }

  Future<void> deleteDirectory(String dirPath) async {
    var content = await listDirectory(dirPath);

    for (final item in content) {
      var path = '$dirPath/${item.filename}';

      if (item.attr.isDirectory) {
        await deleteDirectory(path);
      } else {
        await deleteFile(path);
      }
    }

    await _stfpClient.rmdir(dirPath);
  }

  Future<void> move(String oldPath, String newPath) async {
    await _stfpClient.rename(oldPath, newPath);
  }

  Future<void> rename(String oldPath, String newName) async {
    String newPath =
        oldPath.substring(0, oldPath.lastIndexOf('/') + 1) + newName;

    await move(oldPath, newPath);
  }

  Future<String> readRemoteFile(String remotePath) async {
    var remoteFile = await _stfpClient.open(remotePath);
    var content = await remoteFile.readBytes();
    remoteFile.close();

    return utf8.decode(content, allowMalformed: true);
  }

  Future<void> writeRemoteFile(String remotePath, String data) async {
    var remoteFile = await _stfpClient.open(remotePath,
        mode: SftpFileOpenMode.create |
            SftpFileOpenMode.truncate |
            SftpFileOpenMode.write);

    var bytes = Uint8List.fromList(utf8.encode(data));

    await remoteFile.writeBytes(bytes);
    remoteFile.close();
  }

  Future<void> dispose() async {
    _stfpClient.close();
    _sshClient.close();
    await _sshClient.done;
  }
}
