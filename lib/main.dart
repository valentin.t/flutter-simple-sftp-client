import 'package:flutter/material.dart';

import './theme/style.dart';

import './navigation/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test SFTP',
      theme: appTheme(),
      routes: allRoutes,
      initialRoute: '/'
    );
  }
}