import 'package:flutter/material.dart';

class DialogManager {
  static void showOkDialog(BuildContext context, String title, String message) {
    Map<String, Function()> actions = {'OK': () => Navigator.pop(context)};

    showCustomDialog(context, title, message, actions);
  }

  static void showYesNoDialog(
      BuildContext context, String title, String message, Function() onYes,
      {Function()? onNo, bool dismissible = false}) {
    Map<String, Function()> actions = {
      'Oui': onYes,
      'Non': () => onNo ?? Navigator.pop(context)
    };

    showCustomDialog(context, title, message, actions, dismissible);
  }

  static void showCustomDialog(BuildContext context, String title,
      String message, Map<String, Function()> actions, [bool dismissible = true]) {
    List<TextButton> buttons = List<TextButton>.empty(growable: true);

    actions.forEach((key, value) {
      buttons.add(TextButton(
        onPressed: value,
        child: Text(key),
      ));
    });

    AlertDialog alert = AlertDialog(
        title: Text(title), content: Text(message), scrollable: true, actions: buttons);

    showDialog(
      context: context,
      barrierDismissible: dismissible,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static Future<MapEntry<bool, String>> promptDialog(BuildContext context, String title, String message, [String? defaultValue]) async {
    var controller = TextEditingController(text: defaultValue);
    bool valid = false;

    AlertDialog alert = AlertDialog(
        title: Text(title),
        content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [Text(message), TextField(controller: controller)]
        ),
        actions: [
          TextButton(
              onPressed: () {
                valid = true;
                Navigator.pop(context);
              },
              child: const Text('Valider')),
          TextButton(
              onPressed: () {
                valid = false;
                Navigator.pop(context);
              },
              child: const Text('Annuler'))
        ]);

    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );

    return MapEntry(valid, controller.text);
  }
}
